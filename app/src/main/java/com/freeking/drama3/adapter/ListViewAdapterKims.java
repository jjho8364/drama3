package com.freeking.drama3.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.freeking.drama3.R;
import com.freeking.drama3.item.ListViewItemKims;

import java.util.ArrayList;

public class ListViewAdapterKims extends BaseAdapter {
    LayoutInflater inflater = null;
    private ArrayList<ListViewItemKims> ListViewArr = null;
    private int nListCnt = 0;

    public ListViewAdapterKims(ArrayList<ListViewItemKims> ListViewArr) {
        this.ListViewArr = ListViewArr;
        this.nListCnt = ListViewArr.size();
    }

    static class ViewHolder {
        public TextView channel;
        public TextView title;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            final Context context = parent.getContext();

            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.listview_item_kims, parent, false);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.channel = (TextView)convertView.findViewById(R.id.tv_item_kims_channel);
            viewHolder.title = (TextView)convertView.findViewById(R.id.tv_item_kims_title);

            convertView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder)convertView.getTag();
        ListViewItemKims item = ListViewArr.get(position);
        holder.channel.setText(item.getChannel());
        holder.title.setText(item.getTitle());

        return convertView;
    }

    @Override
    public int getCount() {
        return nListCnt;
    }

    @Override
    public Object getItem(int position) {
        return ListViewArr.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position ;
    }
}
