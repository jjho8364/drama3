package com.freeking.drama3.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.freeking.drama3.R;
import com.freeking.drama3.item.ListviewItemKimsImg;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ListViewAdapterKimsImg extends BaseAdapter {
    LayoutInflater inflater = null;
    private ArrayList<ListviewItemKimsImg> ListViewArr = null;
    private int nListCnt = 0;

    public ListViewAdapterKimsImg(ArrayList<ListviewItemKimsImg> ListViewArr) {
        this.ListViewArr = ListViewArr;
        this.nListCnt = ListViewArr.size();
    }

    static class ViewHolder {
        public ImageView logo;
        public TextView title;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            final Context context = parent.getContext();

            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.listview_item_kims_img, parent, false);

            ListViewAdapterKimsImg.ViewHolder viewHolder = new ListViewAdapterKimsImg.ViewHolder();
            viewHolder.logo= (ImageView)convertView.findViewById(R.id.iv_item_kims_logo);
            viewHolder.title = (TextView)convertView.findViewById(R.id.tv_item_kims_title);

            convertView.setTag(viewHolder);
        }

        ListViewAdapterKimsImg.ViewHolder holder = (ListViewAdapterKimsImg.ViewHolder)convertView.getTag();
        ListviewItemKimsImg item = ListViewArr.get(position);
        if(item.getImgUrl()==null || item.getImgUrl().equals("")){
            holder.logo.setImageResource(R.drawable.noimage);
        } else {
            Picasso.with(parent.getContext()).load(item.getImgUrl()).into(holder.logo);
        }
        holder.title.setText(item.getTitle());

        return convertView;
    }

    @Override
    public int getCount() {
        return nListCnt;
    }

    @Override
    public Object getItem(int position) {
        return ListViewArr.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position ;
    }
}
