package com.freeking.drama3.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.freeking.drama3.R;
import com.freeking.drama3.item.ListViewItemBwin;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ListViewAdapterBwin extends BaseAdapter {
    LayoutInflater inflater = null;
    private ArrayList<ListViewItemBwin> ListViewArr = null;
    private int nListCnt = 0;

    public ListViewAdapterBwin(ArrayList<ListViewItemBwin> ListViewArr) {
        this.ListViewArr = ListViewArr;
        this.nListCnt = ListViewArr.size();
    }

    static class ViewHolder {
        public ImageView kind;
        public TextView time;
        public TextView title;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            final Context context = parent.getContext();

            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.listview_item_bwin, parent, false);

            ListViewAdapterBwin.ViewHolder viewHolder = new ListViewAdapterBwin.ViewHolder();
            viewHolder.kind= (ImageView)convertView.findViewById(R.id.tv_item_bwin_kind);
            viewHolder.time = (TextView)convertView.findViewById(R.id.tv_item_bwin_time);
            viewHolder.title = (TextView)convertView.findViewById(R.id.tv_item_bwin_title);

            convertView.setTag(viewHolder);
        }

        ListViewAdapterBwin.ViewHolder holder = (ListViewAdapterBwin.ViewHolder)convertView.getTag();
        ListViewItemBwin item = ListViewArr.get(position);

        Picasso.with(parent.getContext()).load(item.getImgUrl()).into(holder.kind);
        holder.time.setText(item.getTime());
        holder.title.setText(item.getTitle());

        return convertView;
    }

    @Override
    public int getCount() {
        return nListCnt;
    }

    @Override
    public Object getItem(int position) {
        return ListViewArr.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position ;
    }
}
