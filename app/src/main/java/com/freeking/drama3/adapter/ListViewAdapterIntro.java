package com.freeking.drama3.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.freeking.drama3.R;
import com.freeking.drama3.item.ListViewItemIntro;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ListViewAdapterIntro extends BaseAdapter {
    LayoutInflater inflater = null;
    private ArrayList<ListViewItemIntro> ListViewArr = null;
    private int nListCnt = 0;

    public ListViewAdapterIntro(ArrayList<ListViewItemIntro> ListViewArr) {
        this.ListViewArr = ListViewArr;
        this.nListCnt = ListViewArr.size();
    }

    static class ViewHolder {
        public ImageView kind;
        public TextView intro;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            final Context context = parent.getContext();

            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.listview_item_introapp, parent, false);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.kind= (ImageView)convertView.findViewById(R.id.tv_item_kind);
            viewHolder.intro = (TextView)convertView.findViewById(R.id.tv_item_intro);

            convertView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder)convertView.getTag();
        ListViewItemIntro item = ListViewArr.get(position);

        Picasso.with(parent.getContext()).load(item.getImgUrl()).into(holder.kind);
        holder.intro.setText(item.getIntro());

        return convertView;
    }

    @Override
    public int getCount() {
        return nListCnt;
    }

    @Override
    public Object getItem(int position) {
        return ListViewArr.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position ;
    }

}
