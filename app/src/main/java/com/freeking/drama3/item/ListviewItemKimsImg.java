package com.freeking.drama3.item;

public class ListviewItemKimsImg {
    String imgUrl;
    String title;
    String javascript;

    public ListviewItemKimsImg(String imgUrl, String title, String javascript) {
        this.imgUrl = imgUrl;
        this.title = title;
        this.javascript = javascript;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getJavascript() {
        return javascript;
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }
}
