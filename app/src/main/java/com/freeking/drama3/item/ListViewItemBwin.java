package com.freeking.drama3.item;

public class ListViewItemBwin {
    String imgUrl;
    String time;
    String title;
    String javascript;

    public ListViewItemBwin(String imgUrl, String time, String title, String javascript) {
        this.imgUrl = imgUrl;
        this.time = time;
        this.title = title;
        this.javascript = javascript;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getJavascript() {
        return javascript;
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }
}
