package com.freeking.drama3.item;

public class ListViewItemKims {
    String channel;
    String title;
    String videoId;

    public ListViewItemKims(String channel, String title, String videoId) {
        this.channel = channel;
        this.title = title;
        this.videoId = videoId;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }
}
