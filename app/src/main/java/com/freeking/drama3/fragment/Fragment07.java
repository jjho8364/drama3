package com.freeking.drama3.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.freeking.drama3.R;
import com.freeking.drama3.activity.LiveWebview;
import com.freeking.drama3.adapter.ListViewAdapterKims;
import com.freeking.drama3.item.ListViewItemKims;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class Fragment07 extends Fragment implements View.OnClickListener, LocationListener {
    private final String TAG = " Fragment07 - ";
    private ProgressDialog mProgressDialog;
    private ListView listView;
    private String baseUrl = "";
    private ArrayList<ListViewItemKims> listViewItemArr;

    InputMethodManager inputManager;

    private int adsCnt = 0;
    private InterstitialAd interstitialAd;
    AdRequest adRequest;
    SharedPreferences pref;

    // webview
    private WebView webView;
    private FrameLayout customViewContainer;
    private WebChromeClient.CustomViewCallback customViewCallback;
    private View mCustomView;
    private myWebChromeClient mWebChromeClient;
    private myWebViewClient mWebViewClient;
    private String javascript = "$('#menu_2').click()";
    //private String javascript = "$('#cont_bg').remove()";
    boolean stopFlg = true;

    @SuppressLint("SetJavaScriptEnabled")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment06, container, false);

        //Toast.makeText(getContext(), "테스트 메뉴입니다. 버그 제보 부탁드려요.", Toast.LENGTH_SHORT).show();

        baseUrl = getArguments().getString("baseUrl");
        adsCnt = getArguments().getInt("adsCnt");

        pref= getActivity().getSharedPreferences("pref", MODE_PRIVATE); // 선언
        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));
        adRequest = new AdRequest.Builder().build();
        interstitialAd = new InterstitialAd(getActivity());
        interstitialAd.setAdUnitId("ca-app-pub-9440374750128282/2950802134");
        interstitialAd.loadAd(adRequest);

        listView = (ListView)view.findViewById(R.id.listview);

        inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        /////// webview ///////
        customViewContainer = (FrameLayout) view.findViewById(R.id.customViewContainer);
        webView = (WebView) view.findViewById(R.id.webView);

        mWebViewClient = new myWebViewClient();
        webView.setWebViewClient(mWebViewClient);

        mWebChromeClient = new myWebChromeClient();
        webView.setWebChromeClient(mWebChromeClient);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.addJavascriptInterface(new MyJavascriptInterface(), "Android");
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setSaveFormData(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);

        /*String newUA= "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.101 Safari/537.36";
        webView.getSettings().setUserAgentString(newUA);*/
        webView.getSettings().setMediaPlaybackRequiresUserGesture(false);

        webView.loadUrl(baseUrl);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.show();

        return view;
    }

    ///////////////////////////// webview /////////////////////////

    public boolean inCustomView() {
        return (mCustomView != null);
    }

    public void hideCustomView() {
        mWebChromeClient.onHideCustomView();
    }

    @Override
    public void onPause() {
        super.onPause();    //To change body of overridden methods use File | Settings | File Templates.
        Log.d(TAG, "did pause");
        //webView.onPause();
        //destroyWebView();
    }

    @Override
    public void onResume() {
        super.onResume();    //To change body of overridden methods use File | Settings | File Templates.
        //webView.onResume();
        Log.d(TAG, "did onResume");
    }

    @Override
    public void onStop() {
        super.onStop();    //To change body of overridden methods use File | Settings | File Templates.
        if (inCustomView()) {
            hideCustomView();
        }
        Log.d(TAG, "did onStop");
        //webView.onPause();
        //destroyWebView();
    }

    class myWebChromeClient extends WebChromeClient {
        private Bitmap mDefaultVideoPoster;
        private View mVideoProgressView;

        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
            onShowCustomView(view, callback);    //To change body of overridden methods use File | Settings | File Templates.
        }

        /*@Override
        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            String msg = consoleMessage.message()+"";
            if(msg.contains("console.clear")){

                if(stopFlg){
                    stopFlg = false;
                    webView.loadUrl("javascript:" + javascript);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            webView.loadUrl("javascript:window.Android.getHtml(document.getElementsByTagName('body')[0].innerHTML);");
                        }
                    }, 800);
                }
            }

            return super.onConsoleMessage(consoleMessage);
        }*/

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {

            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
            mCustomView = view;
            webView.setVisibility(View.GONE);
            customViewContainer.setVisibility(View.VISIBLE);
            customViewContainer.addView(view);
            customViewCallback = callback;
        }

        @Override
        public View getVideoLoadingProgressView() {

            if (mVideoProgressView == null) {
                LayoutInflater inflater = LayoutInflater.from(getActivity());
                mVideoProgressView = inflater.inflate(R.layout.video_progress, null);
            }
            return mVideoProgressView;
        }

        @Override
        public void onHideCustomView() {
            super.onHideCustomView();    //To change body of overridden methods use File | Settings | File Templates.
            if (mCustomView == null)
                return;

            webView.setVisibility(View.VISIBLE);
            customViewContainer.setVisibility(View.GONE);

            // Hide the custom view.
            mCustomView.setVisibility(View.GONE);

            // Remove the custom view from its container.
            customViewContainer.removeView(mCustomView);
            customViewCallback.onCustomViewHidden();

            mCustomView = null;
        }
    }

    class myWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);    //To change body of overridden methods use File | Settings | File Templates.
        }
    }

    public class MyJavascriptInterface {
        @JavascriptInterface
        public void getHtml(String html){


            //Log.d(TAG, html);
            if(html != null && !html.equals("")){
                Document doc = Jsoup.parse(html);
                listViewItemArr = null;
                listViewItemArr = new ArrayList<ListViewItemKims>();

                // 리스트 생성
                Elements lists =  doc.select(".bod a.cha");
                for(int i=0 ; i<lists.size() ; i++){
                    String channel = lists.get(i).select(".tit").text();
                    String title = "now : " + lists.get(i).select(".pro").text();
                    String id = lists.get(i).attr("id");

                    // 어댑터 적용
                    ListViewItemKims listViewItemKims = new ListViewItemKims(channel, title, id);
                    listViewItemArr.add(listViewItemKims);
                }
                Message msg = handler.obtainMessage();
                handler.sendMessage(msg);

            }
        }
    }

    final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            //destroyWebView();
            mProgressDialog.dismiss();
            listView.setAdapter(new ListViewAdapterKims(listViewItemArr));

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(adsCnt == 1){
                        adsCnt++;
                        SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                        editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                        editor.commit(); //완료한다.
                        interstitialAd.show();
                    } else {
                        Intent intent = new Intent(getActivity(), LiveWebview.class);
                        intent.putExtra("baseUrl", baseUrl);
                        intent.putExtra("menuId", javascript);
                        intent.putExtra("channel", "$('#" + listViewItemArr.get(position).getVideoId() + "').click()");
                        intent.putExtra("adsCnt", "0");
                        startActivity(intent);
                    }
                }
            });
        }
    };

    public void destroyWebView() {
        webView.clearHistory();
        webView.clearCache(true);
        webView.loadUrl("about:blank");
        webView.onPause();
        webView.removeAllViews();
        webView.destroyDrawingCache();
        webView.destroy();
        webView = null;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onClick(View view) {

    }
}
