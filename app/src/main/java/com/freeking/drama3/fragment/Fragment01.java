package com.freeking.drama3.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.freeking.drama3.R;
import com.freeking.drama3.activity.PlayListActivity;
import com.freeking.drama3.adapter.GridViewAdapterList;
import com.freeking.drama3.item.GridViewItemList;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class Fragment01 extends Fragment implements View.OnClickListener, LocationListener {
    private final String TAG = " Fragment01 - ";
    private ProgressDialog mProgressDialog;
    private GridView gridView;
    private GetGridView getGridView = null;
    private String baseUrl = "";
    private ArrayList<GridViewItemList> listViewItemArr;
    //// paging ////
    private int pageNum = 1;
    private TextView tv_currentPage;
    private TextView tv_lastPage;
    private Button nextBtn;
    private Button preBtn;
    private String lastPage = "1";

    private int adsCnt = 0;
    private InterstitialAd interstitialAd;
    AdRequest adRequest;
    SharedPreferences pref;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment01, container, false);

        baseUrl = getArguments().getString("baseUrl");

        pref= getActivity().getSharedPreferences("pref", MODE_PRIVATE); // 선언
        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));
        adRequest = new AdRequest.Builder().build();
        interstitialAd = new InterstitialAd(getActivity());
        interstitialAd.setAdUnitId("ca-app-pub-9440374750128282/2950802134");
        interstitialAd.loadAd(adRequest);

        gridView = (GridView)view.findViewById(R.id.gridview);

        //// paging ////
        tv_currentPage = (TextView)view.findViewById(R.id.fr01_currentpage);
        tv_lastPage = (TextView)view.findViewById(R.id.fr01_lastpage);
        tv_currentPage.setText("1");
        tv_lastPage.setText("1");
        preBtn = (Button)view.findViewById(R.id.fr01_prebtn);
        nextBtn = (Button)view.findViewById(R.id.fr01_nextbtn);
        preBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);

        getGridView = new GetGridView();
        getGridView.execute();

        return view;
    }

    public class GetGridView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            listViewItemArr = new ArrayList<GridViewItemList>();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;
            try {
                Log.d(TAG, "url : " + baseUrl + pageNum);
                doc = Jsoup.connect(baseUrl + pageNum).timeout(20000).get();
                Elements lists = doc.select(".video-section div.item");

                for(int i=0 ; i<lists.size() ; i++){
                    String title = lists.get(i).select(".item-img a").attr("title");
                    String update = lists.get(i).select(".date").text();
                    String imgUrl = lists.get(i).select(".item-img a img").attr("src");
                    String listUrl = lists.get(i).select(".item-img a").attr("href");

                    GridViewItemList gridViewItemList = new GridViewItemList(title, update, imgUrl, listUrl);
                    listViewItemArr.add(gridViewItemList);
                }
                ////////////// get lat page /////////////
                if(lastPage.equals("1")){
                    Elements pages = doc.select(".pagination li");
                    lastPage = pages.get(pages.size()-2).text().replace(",","");
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(getActivity() != null){
                gridView.setAdapter(new GridViewAdapterList(getActivity(), listViewItemArr, R.layout.gridviewitem_list));

                //// paging ////
                tv_currentPage.setText(pageNum+"");
                tv_lastPage.setText(lastPage);

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        if(adsCnt == 1){
                            adsCnt++;
                            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                            editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                            editor.commit(); //완료한다.
                            interstitialAd.show();
                        } else {
                            Intent intent = new Intent(getActivity(), PlayListActivity.class);
                            intent.putExtra("listUrl", listViewItemArr.get(position).getListUrl());
                            intent.putExtra("title", listViewItemArr.get(position).getTitle());
                            intent.putExtra("imgUrl", listViewItemArr.get(position).getImgUrl());
                            intent.putExtra("adsCnt", "0");
                            startActivity(intent);
                        }
                    }
                });
            }

            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fr01_prebtn :
                if(pageNum != 1){
                    pageNum--;
                    if(getGridView != null){
                        getGridView.cancel(true);
                    }
                    getGridView = new GetGridView();
                    getGridView.execute();
                }
                break;

            case R.id.fr01_nextbtn :
                if(tv_lastPage.getText() != null && !(tv_lastPage.getText().toString().equals("")) && pageNum != Integer.parseInt(tv_lastPage.getText().toString())){
                    pageNum++;
                    if(getGridView != null){
                        getGridView.cancel(true);
                    }
                    getGridView = new GetGridView();
                    getGridView.execute();
                }
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(getGridView != null){
            getGridView.cancel(true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(getGridView != null){
            getGridView.cancel(true);
        }
    }

    public String addHttps(String url){
        String result = "";
        if(url != null && url.length() > 5){
            if(url.substring(0, 4).equals("http")){
                result = url;
            } else {
                result = "https:" + url;
            }
        }

        return result;
    }

}
