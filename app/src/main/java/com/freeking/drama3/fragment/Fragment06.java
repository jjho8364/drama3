package com.freeking.drama3.fragment;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.ConsoleMessage;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.freeking.drama3.R;
import com.freeking.drama3.activity.GetMediaFileActivity;
import com.freeking.drama3.activity.LiveWebview;
import com.freeking.drama3.activity.PlayListActivity;
import com.freeking.drama3.activity.WebviewPlayer;
import com.freeking.drama3.adapter.GridViewAdapterList;
import com.freeking.drama3.adapter.ListViewAdapterKims;
import com.freeking.drama3.adapter.ListViewAdapterKimsImg;
import com.freeking.drama3.item.GridViewItemList;
import com.freeking.drama3.item.ListViewItemKims;
import com.freeking.drama3.item.ListviewItemKimsImg;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import static android.content.Context.DOWNLOAD_SERVICE;
import static android.content.Context.MODE_PRIVATE;

public class Fragment06 extends Fragment implements View.OnClickListener, LocationListener {
    private final String TAG = " Fragment06 - ";
    private ProgressDialog mProgressDialog;
    private GetGridView getGridView = null;
    private ListView listView;
    private String baseUrl = "";
    private String baseClass = "";
    private String baseMenu = "";
    private ArrayList<ListviewItemKimsImg> listViewItemArr;

    InputMethodManager inputManager;

    private int adsCnt = 0;
    private InterstitialAd interstitialAd;
    AdRequest adRequest;
    SharedPreferences pref;

    private String javascript = "";
    private String nextUrl = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment11, container, false);

        baseUrl = getArguments().getString("baseUrl");
        baseClass = getArguments().getString("baseClass");
        baseMenu = getArguments().getString("baseMenu");
        adsCnt = getArguments().getInt("adsCnt");

        pref= getActivity().getSharedPreferences("pref", MODE_PRIVATE); // 선언
        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));
        adRequest = new AdRequest.Builder().build();
        interstitialAd = new InterstitialAd(getActivity());
        interstitialAd.setAdUnitId("ca-app-pub-9440374750128282/2950802134");
        interstitialAd.loadAd(adRequest);

        listView = (ListView)view.findViewById(R.id.listview);

        inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        getGridView = new GetGridView();
        getGridView.execute();

        return view;
    }

    public class GetGridView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            listViewItemArr = new ArrayList<ListviewItemKimsImg>();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;
            try {
                doc = Jsoup.connect(baseUrl).timeout(20000).get();
                Elements lists = doc.select(baseClass + " ul li");

                // kims site
                nextUrl = doc.select(".kims").text();

                for(int i=0 ; i<lists.size() ; i++){
                    String imgUrl = lists.get(i).select("img").attr("src");
                    String title = lists.get(i).select(".title").text();
                    String javascript = "$('#" + lists.get(i).select(".javascript").text() + "').click()";

                    ListviewItemKimsImg item = new ListviewItemKimsImg(imgUrl, title, javascript);
                    listViewItemArr.add(item);
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(getActivity() != null){
                //listView.setAdapter(new GridViewAdapterList(getActivity(), listViewItemArr, R.layout.gridviewitem_list));
                listView.setAdapter(new ListViewAdapterKimsImg(listViewItemArr));

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        if(adsCnt == 1){
                            adsCnt++;
                            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                            editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                            editor.commit(); //완료한다.
                            interstitialAd.show();
                        } else {
                            Intent intent = new Intent(getActivity(), LiveWebview.class);
                            intent.putExtra("baseUrl", nextUrl);
                            intent.putExtra("javascriptMenu", "$('#menu_" + baseMenu + "').click()");
                            intent.putExtra("javascriptId", listViewItemArr.get(position).getJavascript());
                            startActivity(intent);
                        }
                    }
                });
            }

            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onClick(View view) {

    }
}
