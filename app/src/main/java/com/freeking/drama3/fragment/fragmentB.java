package com.freeking.drama3.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.freeking.drama3.R;
import com.freeking.drama3.activity.PlayListActivity;
import com.freeking.drama3.adapter.GridViewAdapterList;
import com.freeking.drama3.item.GridViewItemList;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class fragmentB extends Fragment implements View.OnClickListener, LocationListener {
    private final String TAG = " fragmentB - ";
    private ProgressDialog mProgressDialog;
    private ListView listView;
    private GetGridView getGridView = null;
    private String baseUrl = "";
    private ArrayList<String> videoUrlArr ;
    private ArrayList<String> titleArr ;

    private int adsCnt = 0;
    private InterstitialAd interstitialAd;
    AdRequest adRequest;
    SharedPreferences pref;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmentb, container, false);

        baseUrl = getArguments().getString("baseUrl");

        pref= getActivity().getSharedPreferences("pref", MODE_PRIVATE); // 선언
        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));
        adRequest = new AdRequest.Builder().build();
        interstitialAd = new InterstitialAd(getActivity());
        interstitialAd.setAdUnitId("ca-app-pub-9440374750128282/2950802134");
        interstitialAd.loadAd(adRequest);

        listView = (ListView)view.findViewById(R.id.listview);

        getGridView = new GetGridView();
        getGridView.execute();

        return view;
    }

    public class GetGridView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            videoUrlArr = new ArrayList<String>();
            titleArr = new ArrayList<String>();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;
            try {
                doc = Jsoup.connect(baseUrl).timeout(20000).get();

                Elements lists = doc.select("#liveGrid span");

                for(int i=0 ; i<lists.size() ; i++){
                    String videoUrl = lists.get(i).attr("data-url");
                    String title = lists.get(i).attr("data-name");

                    videoUrlArr.add(videoUrl);
                    titleArr.add(title);
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(getActivity() != null){
                listView.setAdapter(new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, titleArr));

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        if(adsCnt == 1){
                            adsCnt++;
                            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                            editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                            editor.commit(); //완료한다.
                            interstitialAd.show();
                        } else {
                            startMxPlayer(videoUrlArr.get(position));
                        }
                    }
                });
            }

            mProgressDialog.dismiss();
        }
    }

    public void startMxPlayer(String videoUrl){
        String packageName = "";

        Intent startLink1 = getActivity().getPackageManager().getLaunchIntentForPackage("com.mxtech.videoplayer.ad");
        Intent startLink2 = getActivity().getPackageManager().getLaunchIntentForPackage("com.mxtech.videoplayer.pro");

        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri videoUri = Uri.parse(videoUrl);
        intent.setDataAndType(videoUri, "video/h264");
        intent.putExtra("decode_mode", (byte) 2);
        intent.putExtra("video_zoom", 0);
        if (startLink2 != null) {
            packageName = "com.mxtech.videoplayer.pro";
        } else {
            packageName = "com.mxtech.videoplayer.ad";
        }
        intent.setPackage(packageName);
        startActivity(intent);
    }
    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onClick(View view) {

    }
}
