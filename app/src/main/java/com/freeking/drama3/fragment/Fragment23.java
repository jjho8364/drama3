package com.freeking.drama3.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.freeking.drama3.R;
import com.freeking.drama3.activity.MovieListActivity;
import com.freeking.drama3.activity.PlayListActivity;
import com.freeking.drama3.adapter.GridViewAdapterList;
import com.freeking.drama3.item.GridViewItemList;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class Fragment23 extends Fragment implements View.OnClickListener, LocationListener {
    private final String TAG = " Fragment23 - ";
    private ProgressDialog mProgressDialog;
    private GridView gridView;
    private GetGridView getGridView = null;
    private GetGridView getGridView2 = null;
    private String baseUrl = "";
    private ArrayList<GridViewItemList> listViewItemArr;

    //// paging ////
    private int pageNum = 1;
    private TextView tv_currentPage;
    private TextView tv_lastPage;
    private Button nextBtn;
    private Button preBtn;
    private String lastPage = "1";

    // search
    private String searchUrl = "?sfl=wr_subject&stx=";
    private String baseEndUrl = "&sop=and&page=";
    private String keyword1 = "";
    private EditText editText;
    private Button searchBtn;
    InputMethodManager inputManager;

    private int adsCnt = 0;
    private InterstitialAd interstitialAd;
    AdRequest adRequest;
    SharedPreferences pref;

    int searchingMod = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment23, container, false);

        baseUrl = getArguments().getString("baseUrl");

        pref= getActivity().getSharedPreferences("pref", MODE_PRIVATE); // 선언
        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));
        adRequest = new AdRequest.Builder().build();
        interstitialAd = new InterstitialAd(getActivity());
        interstitialAd.setAdUnitId("ca-app-pub-9440374750128282/2950802134");
        interstitialAd.loadAd(adRequest);

        gridView = (GridView)view.findViewById(R.id.gridview);

        // search
        editText = (EditText)view.findViewById(R.id.fr20_edit);
        searchBtn = (Button)view.findViewById(R.id.fr20_searchbtn);
        searchBtn.setOnClickListener(this);
        inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        //// paging ////
        tv_currentPage = (TextView)view.findViewById(R.id.fr01_currentpage);
        tv_lastPage = (TextView)view.findViewById(R.id.fr01_lastpage);
        tv_currentPage.setText("1");
        tv_lastPage.setText("1");
        preBtn = (Button)view.findViewById(R.id.fr01_prebtn);
        nextBtn = (Button)view.findViewById(R.id.fr01_nextbtn);
        preBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);

        getGridView = new GetGridView();
        getGridView.execute();

        return view;
    }

    public class GetGridView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            listViewItemArr = new ArrayList<GridViewItemList>();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                if(searchingMod == 1){  // normal
                    Log.d(TAG, "url : " + baseUrl + pageNum);
                    doc = Jsoup.connect(baseUrl + pageNum).timeout(20000).get();
                } else {    // searching
                    Log.d(TAG, "url : " + baseUrl + searchUrl + keyword1 + baseEndUrl + pageNum);
                    doc = Jsoup.connect(baseUrl + searchUrl + keyword1 + baseEndUrl + pageNum).timeout(20000).get();
                }

                Elements lists = doc.select(".list-row");

                for(int i=0 ; i<lists.size() ; i++){
                    String title = lists.get(i).select(".en").text();
                    String update = "";
                    String imgUrl = lists.get(i).select(".img-item img").attr("src");
                    String listUrl = lists.get(i).select(".list-desc a").attr("href");

                    GridViewItemList gridViewItemList = new GridViewItemList(title, update, imgUrl, listUrl);
                    listViewItemArr.add(gridViewItemList);
                }
                ////////////// get lat page /////////////
                if(lastPage.equals("1")){
                    Elements pages = doc.select(".pagination li");

                    String[] tempArr = pages.get(pages.size()-1).select("a").attr("href").split("page=");
                    if(tempArr.length > 1) {
                        lastPage = pages.get(pages.size()-1).select("a").attr("href").split("page=")[1];
                    } else {
                        lastPage = pages.get(pages.size()-2).select("a").attr("href").split("page=")[1];
                    }
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(getActivity() != null){
                gridView.setAdapter(new GridViewAdapterList(getActivity(), listViewItemArr, R.layout.gridviewitem_list));

                //// paging ////
                tv_currentPage.setText(pageNum+"");
                tv_lastPage.setText(lastPage);

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        if(adsCnt == 1){
                            adsCnt++;
                            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                            editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                            editor.commit(); //완료한다.
                            interstitialAd.show();
                        } else {
                            Intent intent = new Intent(getActivity(), MovieListActivity.class);
                            intent.putExtra("listUrl", listViewItemArr.get(position).getListUrl());
                            intent.putExtra("title", listViewItemArr.get(position).getTitle());
                            intent.putExtra("imgUrl", listViewItemArr.get(position).getImgUrl());
                            intent.putExtra("adsCnt", "0");
                            startActivity(intent);
                        }
                    }
                });
            }

            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fr20_searchbtn :
                if(!editText.getText().toString().equals("")){
                    searchingMod = 2;
                    lastPage = "1";
                    pageNum = 1;

                    keyword1 = editText.getText().toString().trim();
                    inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    getGridView = new GetGridView();
                    getGridView.execute();

                } else {
                    Toast.makeText(getActivity(), "검색어를 입력하세요.", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.fr01_prebtn :
                if(pageNum != 1){
                    pageNum--;
                    if(getGridView != null){
                        getGridView.cancel(true);
                    }
                    getGridView = new GetGridView();
                    getGridView.execute();
                }
                break;

            case R.id.fr01_nextbtn :
                if(tv_lastPage.getText() != null && !(tv_lastPage.getText().toString().equals("")) && pageNum != Integer.parseInt(tv_lastPage.getText().toString())){
                    pageNum++;
                    if(getGridView != null){
                        getGridView.cancel(true);
                    }
                    getGridView = new GetGridView();
                    getGridView.execute();
                }
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(getGridView != null){
            getGridView.cancel(true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(getGridView != null){
            getGridView.cancel(true);
        }
    }

    public String addHttps(String url){
        String result = "";
        if(url != null && url.length() > 5){
            if(url.substring(0, 4).equals("http")){
                result = url;
            } else {
                result = "https:" + url;
            }
        }

        return result;
    }

}
