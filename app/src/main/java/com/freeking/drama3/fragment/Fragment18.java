package com.freeking.drama3.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.freeking.drama3.R;
import com.freeking.drama3.activity.PlayListMidActivity;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class Fragment18 extends Fragment implements View.OnClickListener, LocationListener {
    private final String TAG = " Fragment18 - ";
    private ProgressDialog mProgressDialog;
    private ListView listView;
    private GetListView getListView = null;
    private String baseUrl = "";
    private String keyword1 = "";
    private String keyword2 = "";
    private String baseEndUrl = "%7C6/page/";
    private ArrayList<String> pageUrlArr;
    private ArrayList<String> titleArr;

    private int pageNum = 1;
    private TextView tv_currentPage;
    private TextView tv_lastPage;
    private Button nextBtn;
    private Button preBtn;

    private EditText editText;
    private Button searchBtn;

    InputMethodManager inputManager;

    private int adsCnt = 0;
    private InterstitialAd interstitialAd;
    AdRequest adRequest;
    SharedPreferences pref;

    ArrayAdapter<String> adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mid_search, container, false);

        baseUrl = getArguments().getString("baseUrl");

        pref= getActivity().getSharedPreferences("pref", MODE_PRIVATE); // 선언
        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));
        adRequest = new AdRequest.Builder().build();
        interstitialAd = new InterstitialAd(getActivity());
        interstitialAd.setAdUnitId("ca-app-pub-9440374750128282/2950802134");
        interstitialAd.loadAd(adRequest);

        listView = (ListView)view.findViewById(R.id.listview);

        Log.d(TAG, "baseUrl : " + baseUrl);

        tv_currentPage = (TextView)view.findViewById(R.id.fr01_currentpage);
        tv_lastPage = (TextView)view.findViewById(R.id.fr01_lastpage);
        tv_currentPage.setText("1");
        tv_lastPage.setText("1");
        preBtn = (Button)view.findViewById(R.id.fr01_prebtn);
        nextBtn = (Button)view.findViewById(R.id.fr01_nextbtn);

        preBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);

        editText = (EditText)view.findViewById(R.id.fr20_edit);
        searchBtn = (Button)view.findViewById(R.id.fr20_searchbtn);
        searchBtn.setOnClickListener(this);

        inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        return view;
    }

    public class GetListView extends AsyncTask<Void, Void, Void> {

        String tempLastPage = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pageUrlArr = new ArrayList<String>();
            titleArr = new ArrayList<String>();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                doc = Jsoup.connect(baseUrl + keyword1 + "+" + keyword2 + baseEndUrl + pageNum).timeout(10000).get();

                Elements lists = doc.select(".media-heading");

                for(int i=0 ; i<lists.size() ; i++){
                    String title = lists.get(i).select("a").text();
                    String tempUrl = lists.get(i).select("a").attr("href");
                    String[] strArr = java.net.URLDecoder.decode(tempUrl, "UTF-8").split("/");
                    String pageUrl = "";
                    for(int j=0 ; j<strArr.length ; j++){
                        if(j == 6){
                            pageUrl += "spall/";
                        }
                        if(j != strArr.length-1){
                            pageUrl += strArr[j] + "/";
                        } else {
                            pageUrl += strArr[j];
                        }
                    }
                    titleArr.add(title);
                    pageUrlArr.add(pageUrl);
                }

                ////////////// get lat page /////////////
                Elements lis = doc.select(".pagination");
                if(lis.size() != 0){
                    String[] tempStrArr = lis.select("li").last().select("a").attr("href").split("=");
                    Log.d(TAG, "tempStrArr length : " + tempStrArr.length);
                    if(tempStrArr.length > 1){
                        tempLastPage = tempStrArr[tempStrArr.length-1];
                        Log.d(TAG, "Last Page : " + tempLastPage);
                    } else {
                        tempLastPage = "1";
                    }
                }
            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(getActivity() != null){
                if(titleArr.size() == 0){
                    keyword1 = "";
                    keyword2 = "";
                    titleArr.clear();
                    adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, titleArr);
                    listView.setAdapter(adapter);
                } else {
                    tv_currentPage.setText(pageNum+"");
                    tv_lastPage.setText(tempLastPage);

                    adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, titleArr);
                    listView.setAdapter(adapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            if(adsCnt == 1){
                                adsCnt++;
                                SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                                editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                                editor.commit(); //완료한다.
                                interstitialAd.show();
                            } else {
                                Intent intent = new Intent(getActivity(), PlayListMidActivity.class);
                                intent.putExtra("listUrl", pageUrlArr.get(position));
                                intent.putExtra("adsCnt", "0");
                                startActivity(intent);
                            }
                        }
                    });
                }
            }

            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fr01_prebtn :
                if(pageNum != 1 && !keyword1.equals("")){
                    pageNum--;
                    getListView = new GetListView();
                    getListView.execute();
                }
                break;

            case R.id.fr01_nextbtn :
                if(tv_lastPage.getText() != null && !(tv_lastPage.getText().toString().equals("")) && pageNum != Integer.parseInt(tv_lastPage.getText().toString()) && !keyword1.equals("")){
                    pageNum++;
                    getListView = new GetListView();
                    getListView.execute();
                }
                break;
            case R.id.fr20_searchbtn :
                if(!editText.getText().toString().equals("")){
                    String[] searchWord = editText.getText().toString().trim().split(" ");
                    if(searchWord.length > 2){
                        keyword1 = "";
                        keyword2 = "";
                        Toast.makeText(getActivity(), "띄어쓰기는 한번만 사용해 주세요.", Toast.LENGTH_LONG).show();
                    } else if(searchWord.length == 1){
                        keyword1 = searchWord[0];
                        keyword2 = "";
                    } else {
                        keyword1 = searchWord[0];
                        keyword2 = searchWord[1];
                    }
                    inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    pageNum = 1;
                    getListView = new GetListView();
                    getListView.execute();

                } else {
                    Toast.makeText(getActivity(), "검색어를 입력하세요.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(getListView != null){
            getListView.cancel(true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(getListView != null){
            getListView.cancel(true);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

}
