package com.freeking.drama3.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.freeking.drama3.R;
import com.freeking.drama3.adapter.ListViewMidAdapterPlayBtn;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class MovieList2Activity extends Activity {
    private String TAG = " MovieListActivity - ";
    private ProgressDialog mProgressDialog;
    ListView listView;
    private GetListView getListView = null;
    private GetPlayer getPlayer = null;
    private String baseUrl = "";

    private ImageView posterView;
    private TextView titleView;
    private TextView genreView;
    private TextView nationView;
    private TextView openView;
    private TextView infoView;
    private TextView directorView;
    private TextView actorView;
    private TextView storyView;
    private String playerUrl = "";
    private String nextUrl = "";

    private ListView btnListView;

    private int adsCnt = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_movie_list);

        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        Intent intent = getIntent();
        baseUrl = intent.getStringExtra("listUrl");
        String imgUrl = intent.getStringExtra("imgUrl");
        adsCnt = Integer.parseInt(intent.getStringExtra("adsCnt"));

        posterView = (ImageView)findViewById(R.id.iv_list_poster);
        titleView = (TextView)findViewById(R.id.tv_list_title);
        genreView = (TextView)findViewById(R.id.tv_list_genre);
        nationView = (TextView)findViewById(R.id.tv_list_nation);
        openView = (TextView)findViewById(R.id.tv_list_open);
        infoView = (TextView)findViewById(R.id.tv_list_info);
        directorView = (TextView)findViewById(R.id.tv_list_director);
        actorView = (TextView)findViewById(R.id.tv_list_actor);
        storyView = (TextView)findViewById(R.id.tv_movie_story);

        listView = (ListView)findViewById(R.id.list_listview);
        btnListView = (ListView)findViewById(R.id.list_btn_view);

        if(imgUrl != null && !imgUrl.equals("")){
            Picasso.with(MovieList2Activity.this).load(imgUrl).into(posterView);
        } else {
            posterView.setImageResource(R.drawable.no_image_png);
        }

        getListView = new GetListView();
        getListView.execute();
    }

    public class GetListView extends AsyncTask<Void, Void, Void> {

        String imgUrl = "";
        String title = "";
        String genre = "";
        String nation = "";
        String open = "";
        String info = "";
        String director = "";
        String actor = "";
        String story = "";

        ArrayList<String> btnTextArr = new ArrayList<String>();
        List<String> btnVideoUrlArr = new ArrayList<String>();

        List<String> listTitleArr = new ArrayList<String>();
        List<String> listPageUrlArr = new ArrayList<String>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(MovieList2Activity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                doc = Jsoup.connect(baseUrl).timeout(15000).get();

                //////////////// episode contents ////////////
                //imgUrl = doc.select(".img_summary").attr("src");
                title = doc.select(".data h1").text();
                genre = doc.select(".sgeneros").text();
                nation = doc.select(".country").text();
                open = doc.select(".date").text();
                info = doc.select(".runtime").text();
                director = "";
                actor = "";
                story =  doc.select(".wp-content p").text();

                /////////////// video button //////////////
                Elements lists = doc.select(".fix-table a");

                int linkNum = 1;
                for(int i=0 ; i<lists.size() ; i++) {
                    String btnVideoUrl = lists.get(i).attr("href");

                    if(btnVideoUrl.contains(".daum.net/")) continue;
                    if(btnVideoUrl.contains("hqvid.net")) continue;
                    if(btnVideoUrl.contains("goovid.net")) continue;
                    //if(btnVideoUrl.contains("openload")) continue;
                    btnTextArr.add("Link " + linkNum++);
                    btnVideoUrlArr.add(btnVideoUrl);
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            titleView.setText(title);
            genreView.setText(genre);
            nationView.setText(nation);
            openView.setText(open);
            infoView.setText(info);
            directorView.setText(director);
            actorView.setText(actor);
            storyView.setText(story);

            ///////// set button list ////////////
            btnListView.setAdapter(new ListViewMidAdapterPlayBtn(btnTextArr));
            btnListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    nextUrl = btnVideoUrlArr.get(position);
                    getPlayer = new GetPlayer();
                    getPlayer.execute();
                }
            });

            mProgressDialog.dismiss();
        }
    }


    public class GetPlayer extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            playerUrl = "";

            mProgressDialog = new ProgressDialog(MovieList2Activity.this);
            mProgressDialog.setTitle("플레이어를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                doc = Jsoup.connect(nextUrl).timeout(15000).get();

                playerUrl = doc.select("#player #vid_iframe").attr("src");

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Intent intent = new Intent(MovieList2Activity.this, GetMediaFileActivity.class);
            intent.putExtra("baseUrl", playerUrl);
            startActivity(intent);

            mProgressDialog.dismiss();
        }
    }

}
