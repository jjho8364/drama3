package com.freeking.drama3.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.freeking.drama3.R;
import com.freeking.drama3.adapter.GridViewAdapterList;
import com.freeking.drama3.adapter.ListViewAdapterPlayBtn;
import com.freeking.drama3.item.GridViewItemList;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class PlayListActivity extends Activity {
    private String TAG = " PlayListActivity - ";
    private ProgressDialog mProgressDialog;
    private GetGridView getGridView = null;
    private String baseUrl = "";
    private String title = "";
    private String imgUrl = "";
    private TextView tv_title;
    private ImageView img_poster;

    private ArrayList<GridViewItemList> listViewItemArr;
    private GridView gridView;
    private ListView btnListView;

    private String nextUrl = "";
    private GetPlayerUrl getPlayerUrl;

    private int adsCnt = 0;
    private String listUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_play_list);

        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        Intent intent = getIntent();
        baseUrl = intent.getStringExtra("listUrl");
        title = intent.getStringExtra("title");
        imgUrl = intent.getStringExtra("imgUrl");
        adsCnt = Integer.parseInt(intent.getStringExtra("adsCnt"));

        tv_title = (TextView)findViewById(R.id.tv_title);
        img_poster  = (ImageView)findViewById(R.id.img_poster);

        tv_title.setText(title);
        if (imgUrl != null && !imgUrl.equals("")){
            Picasso.with(this).load(imgUrl).into(img_poster);
        }

        gridView = (GridView)findViewById(R.id.gridview);
        btnListView = (ListView)findViewById(R.id.list_btn_view);

        getGridView = new GetGridView();
        getGridView.execute();

    }

    public void refresh(String listUrl){
        Intent intent = new Intent(PlayListActivity.this, PlayListActivity.class);
        intent.putExtra("listUrl", listUrl);
        intent.putExtra("title", title);
        intent.putExtra("imgUrl", imgUrl);
        intent.putExtra("adsCnt", ""+adsCnt);
        startActivity(intent);
        finish();
    }

    public class GetPlayerUrl extends AsyncTask<Void, Void, Void> {

        String playerUrl = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(PlayListActivity.this);
            mProgressDialog.setTitle("플레이어를 찾는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }
        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;

            try {
                doc = Jsoup.connect(nextUrl).timeout(15000).get();
                playerUrl = doc.select(".player iframe").attr("src");

                if(playerUrl == null || playerUrl.equals("")){
                    playerUrl = doc.select(".div-container .div-center a").attr("href");
                }

                if(playerUrl.contains("media.videomovil")){
                    doc = Jsoup.connect(playerUrl).timeout(15000).get();
                    playerUrl = doc.select("iframe").first().attr("src");
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Intent intent = new Intent(PlayListActivity.this, GetMediaFileActivity.class);
            intent.putExtra("baseUrl", playerUrl);
            startActivity(intent);

            mProgressDialog.dismiss();
        }
    }

    public class GetGridView extends AsyncTask<Void, Void, Void> {

        ArrayList<String> btnTextArr = new ArrayList<String>();
        ArrayList<String> btnVideoUrlArr = new ArrayList<String>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            listViewItemArr = new ArrayList<GridViewItemList>();

            mProgressDialog = new ProgressDialog(PlayListActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                doc = Jsoup.connect(baseUrl).timeout(15000).get();

                Elements lists = doc.select(".carousel-inner .item.active .item");

                for(int i=0 ; i<lists.size() ; i++){
                    String title = lists.get(i).select("h3 a").attr("title");
                    String update = lists.get(i).select(".date").text();
                    String imgUrl = lists.get(i).select(".item-img a img").attr("src");
                    String listUrl = lists.get(i).select(".item-img a").attr("href");

                    GridViewItemList gridViewItemList = new GridViewItemList(title, update, imgUrl, listUrl);
                    listViewItemArr.add(gridViewItemList);
                }

                /////////////// video button //////////////
                Elements btns = doc.select(".pagination.post-tape a");

                for(int i=0 ; i<btns.size() ; i++){
                    String text = btns.get(i).text();
                    String videoUrl = btns.get(i).attr("href");

                    if(videoUrl.contains(".daum.net/")) continue;
                    if(videoUrl.contains("hqvid.net")) continue;
                    if(videoUrl.contains("goovid.net")) continue;
                    //if(videoUrl.contains("openload")) continue;
                    btnTextArr.add(text);
                    btnVideoUrlArr.add(videoUrl);
                }

                if(btnTextArr.size() == 0){
                    String btnVideoUrl = doc.select(".player iframe").attr("src");
                    Log.d(TAG, "btnVideoUrl : " + btnVideoUrl);

                    if(btnVideoUrl != null && !btnVideoUrl.equals("")) {
                        btnTextArr.add("링크 1");
                        btnVideoUrlArr.add(baseUrl);
                    }
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(PlayListActivity.this != null && listViewItemArr.size() != 0){
                gridView.setAdapter(new GridViewAdapterList(PlayListActivity.this, listViewItemArr, R.layout.gridviewitem_list));

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        adsCnt++;
                        Log.d(TAG, "adsCnt : " + adsCnt);
                        listUrl = listViewItemArr.get(position).getListUrl();
                        title = listViewItemArr.get(position).getTitle();
                        imgUrl = listViewItemArr.get(position).getImgUrl();
                        refresh(listUrl);
                    }
                });
            }

            ///////// set button list ////////////
            if(btnTextArr.size() == 0){
                Toast.makeText(PlayListActivity.this, "방송 준비 중입니다. 1시간 내에 완료됩니다.", Toast.LENGTH_SHORT).show();
            }
            btnListView.setAdapter(new ListViewAdapterPlayBtn(btnTextArr));
            btnListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    nextUrl = btnVideoUrlArr.get(position);
                    getPlayerUrl = new GetPlayerUrl();
                    getPlayerUrl.execute();
                }
            });

            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        destroyAsync();
    }

    public void destroyAsync(){
        if(getGridView != null){
            getGridView.cancel(true);
        }
        if(getPlayerUrl != null){
            getPlayerUrl.cancel(true);
        }
    }
}
