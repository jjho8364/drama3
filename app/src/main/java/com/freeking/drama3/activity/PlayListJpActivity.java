package com.freeking.drama3.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.freeking.drama3.R;
import com.freeking.drama3.adapter.ListViewMidAdapterPlayBtn;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class PlayListJpActivity extends Activity {
    private String TAG = " PlayListJpActivity - ";
    private ProgressDialog mProgressDialog;
    ListView listView;
    private GetListView getListView = null;
    private String baseUrl = "";

    private ImageView posterView;
    private TextView titleView;
    private TextView storyView;

    private ListView btnListView;

    private int adsCnt = 0;

    private String listUrl = "";
    private String nextUrl = "";
    private GetPlayerUrl getPlayerUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_play_list_jp);

        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        Intent intent = getIntent();
        baseUrl = intent.getStringExtra("listUrl");
        adsCnt = Integer.parseInt(intent.getStringExtra("adsCnt"));

        posterView = (ImageView)findViewById(R.id.iv_list_eps);
        titleView = (TextView)findViewById(R.id.tv_list_eps);
        storyView = (TextView)findViewById(R.id.tv_list_story);

        listView = (ListView)findViewById(R.id.list_listview);
        btnListView = (ListView)findViewById(R.id.list_btn_view);

        getListView = new GetListView();
        getListView.execute();

    }

    public class GetListView extends AsyncTask<Void, Void, Void> {

        String imgUrl = "";
        String title = "";
        String story = "";

        ArrayList<String> btnTextArr = new ArrayList<String>();
        List<String> btnVideoUrlArr = new ArrayList<String>();

        List<String> listTitleArr = new ArrayList<String>();
        List<String> listPageUrlArr = new ArrayList<String>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(PlayListJpActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                String[] tempStr = baseUrl.split("/");
                baseUrl = "";
                for(int i=0 ; i<tempStr.length ; i++){
                    if(i == tempStr.length-1){
                        baseUrl += URLEncoder.encode( tempStr[i], "utf-8" );
                    } else {
                        baseUrl += tempStr[i] + "/";
                    }
                }
                Log.d(TAG, "baseUrl : " + baseUrl);
                doc = Jsoup.connect(baseUrl).timeout(15000).get();

                ////////////////// episode list //////////////
                Elements lists = doc.select(".list-group .list-group-item");

                for(int i=0 ; i<lists.size() ; i++){
                    String listTitle = lists.get(i).text();
                    String listVideoUrl = "https://baydrama.co" + lists.get(i).attr("href");
                    listTitleArr.add(listTitle);
                    listPageUrlArr.add(listVideoUrl);
                }
                //////////////// episode contents ////////////
                imgUrl = "https:" + doc.select("center:eq(2) img").attr("src");
                Log.d(TAG, "imgUrl : " + imgUrl);
                title = doc.select("h1.page-header").text();
                story = "판도라TV는 광고를 제거 할수 없습니다.";

                /////////////// video button //////////////
                Elements btns = doc.select(".container center a");

                for(int i=0 ; i<btns.size() ; i++){
                    String btnText = btns.get(i).text();
                    String btnVideoUrl = btns.get(i).attr("href");

                    if(btnText.contains("전체목록")) continue;
                    if(btnText.contains("다음회차")) continue;
                    if(btnText.contains("videofarm.daum.net")) continue;
                    if(btnText.contains("k-vid.net")) continue;
                    if(btnText.contains("thevideo.me")) continue;
                    if(btnText.contains("current")) break;

                    if(btnVideoUrl.contains(".daum.net/")) continue;
                    if(btnVideoUrl.contains("hqvid.net")) continue;
                    if(btnVideoUrl.contains("goovid.net")) continue;
                    //if(btnVideoUrl.contains("openload")) continue;

                    btnTextArr.add(btnText);
                    btnVideoUrlArr.add(btnVideoUrl);

                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(!(imgUrl == null || imgUrl.equals(""))){
                Log.d(TAG, "imgUrl2 : " + imgUrl);
                Picasso.with(PlayListJpActivity.this).load(imgUrl).into(posterView);
            }
            titleView.setText(title);
            storyView.setText(story);

            //////////// set listview ///////////
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(PlayListJpActivity.this, android.R.layout.simple_list_item_1, listTitleArr);
            listView.setAdapter(adapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    listUrl = listPageUrlArr.get(position);
                    refresh(listUrl);
                }
            });

            ///////// set button list ////////////
            btnListView.setAdapter(new ListViewMidAdapterPlayBtn(btnTextArr));
            btnListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    nextUrl = btnVideoUrlArr.get(position);
                    getPlayerUrl = new GetPlayerUrl();
                    getPlayerUrl.execute();

                    /*Intent intent = new Intent(PlayListActivity2.this, GetMediaFileActivity.class);
                    intent.putExtra("baseUrl", btnVideoUrlArr.get(position));
                    startActivity(intent);*/
                }
            });

            mProgressDialog.dismiss();
        }
    }

    public class GetPlayerUrl extends AsyncTask<Void, Void, Void> {

        String playerUrl = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(PlayListJpActivity.this);
            mProgressDialog.setTitle("플레이어를 찾는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }
        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;

            try {
                doc = Jsoup.connect(nextUrl).timeout(15000).get();
                playerUrl = doc.select("iframe").attr("src");
                Log.d(TAG, "playerUrl : " + playerUrl);

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(playerUrl.contains("k-vid")){
                Toast.makeText(PlayListJpActivity.this, "이 링크는 사용할 수 없습니다.", Toast.LENGTH_LONG).show();
            } else {
                Intent intent;
                if(playerUrl.contains("pandora")){
                    intent = new Intent(PlayListJpActivity.this, WebviewPlayer.class);
                    intent.putExtra("videoUrl", playerUrl);
                    Toast.makeText(PlayListJpActivity.this, "판도라TV는 광고를 제거 할 수 없습니다.", Toast.LENGTH_LONG).show();
                } else {
                    intent = new Intent(PlayListJpActivity.this, GetMediaFileActivity.class);
                    intent.putExtra("baseUrl", playerUrl);
                }
                startActivity(intent);
            }

            mProgressDialog.dismiss();
        }
    }

    public void refresh(String listUrl){
        Intent intent = new Intent(PlayListJpActivity.this, PlayListJpActivity.class);
        intent.putExtra("listUrl",  listUrl);
        intent.putExtra("adsCnt",  adsCnt+"");
        finish();
        startActivity(intent);
    }

}
