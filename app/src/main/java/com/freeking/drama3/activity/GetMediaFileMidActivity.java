package com.freeking.drama3.activity;

import android.app.DownloadManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.MimeTypeMap;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.freeking.drama3.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class GetMediaFileMidActivity extends AppCompatActivity {
    private String TAG = " GetMediaFileActivity - ";
    private WebView webView;
    private FrameLayout customViewContainer;
    private WebChromeClient.CustomViewCallback customViewCallback;
    private View mCustomView;
    private myWebChromeClient mWebChromeClient;
    private myWebViewClient mWebViewClient;
    private String baseUrl = "";
    private String javascript = "";
    private boolean autoCek = false;
    private String fileType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        setContentView(R.layout.activity_get_media_file);

        baseUrl = getIntent().getStringExtra("baseUrl");
        javascript = getJavascript(baseUrl);
        fileType = getFileType(baseUrl);

        ImageView loadingImg = (ImageView)findViewById(R.id.gif_dog);
        GlideDrawableImageViewTarget gifImage = new GlideDrawableImageViewTarget(loadingImg);
        Glide.with(this).load(R.drawable.sleepy_dog).into(gifImage);

        customViewContainer = (FrameLayout) findViewById(R.id.customViewContainer);
        webView = (WebView) findViewById(R.id.webView);

        webView.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "download");
                DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                dm.enqueue(request);
            }
        });

        mWebViewClient = new myWebViewClient();
        webView.setWebViewClient(mWebViewClient);

        mWebChromeClient = new myWebChromeClient();
        webView.setWebChromeClient(mWebChromeClient);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.addJavascriptInterface(new MyJavascriptInterface(), "Android");
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setSaveFormData(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);

        String newUA= "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0";
        webView.getSettings().setUserAgentString(newUA);
        webView.getSettings().setMediaPlaybackRequiresUserGesture(false);

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {

                if(url.contains(".gif") || url.contains(".png") || url.contains(".jpg")){

                } else if(autoCek && fileType.equals("pandora.fuck") && url.contains("trans-idx.gcdn2.pandora.tv/flvemx2.pandora.tv/")){
                    startMxPlayer(url);
                } else {
                    if(autoCek && !(fileType.equals("webview")) && !(fileType.equals("html")) && url.contains(fileType) ){
                        if(fileType.equals(".m3u8")){
                            if(url.contains("proxy")){
                                startMxPlayer(url.split("#")[0]);  // fucking daily motion
                            }
                        } else {
                            startMxPlayer(url.split("#")[0]);  // fucking daily motion
                        }
                    }
                }

                return super.shouldInterceptRequest(view, url);
            }
            /////////////////////////////////////////////////////////////////////////////////////
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i(TAG, "Processing webview url click..." + url);
                view.stopLoading();
                if(fileType.equals("html")){
                    Intent intent = new Intent(GetMediaFileMidActivity.this, WebviewPlayer.class);
                    intent.putExtra("videoUrl",url);
                    startActivity(intent);
                    finish();
                } else {
                    view.loadUrl(url);
                }
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(webView, url);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                if(fileType.equals("html")){ // get url address
                    view.loadUrl("javascript:window.Android.getHtml(document.getElementsByTagName('body')[0].innerHTML);");
                } else if(!(fileType.equals("webview"))){
                    webView.loadUrl("javascript:" + javascript);
                    autoCek = true;
                }
            }
        });

        webView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {

            }
        });
        webView.loadUrl(baseUrl);
    }



    public boolean inCustomView() {
        return (mCustomView != null);
    }

    public void hideCustomView() {
        mWebChromeClient.onHideCustomView();
    }

    @Override
    protected void onPause() {
        super.onPause();    //To change body of overridden methods use File | Settings | File Templates.
        //webView.onPause();
        if(webView != null) destroyWebView();
    }

    @Override
    protected void onResume() {
        super.onResume();    //To change body of overridden methods use File | Settings | File Templates.
        if(webView != null) webView.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();    //To change body of overridden methods use File | Settings | File Templates.
        if (inCustomView()) {
            hideCustomView();
        }
        if(webView != null) destroyWebView();
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (inCustomView()) {
                hideCustomView();
                return true;
            }

            if ((mCustomView == null) && webView.canGoBack()) {
                webView.goBack();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    class myWebChromeClient extends WebChromeClient {
        private Bitmap mDefaultVideoPoster;
        private View mVideoProgressView;

        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
            onShowCustomView(view, callback);    //To change body of overridden methods use File | Settings | File Templates.
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {

            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
            mCustomView = view;
            if(webView != null) webView.setVisibility(View.GONE);
            customViewContainer.setVisibility(View.VISIBLE);
            customViewContainer.addView(view);
            customViewCallback = callback;
        }

        @Override
        public View getVideoLoadingProgressView() {

            if (mVideoProgressView == null) {
                LayoutInflater inflater = LayoutInflater.from(GetMediaFileMidActivity.this);
                mVideoProgressView = inflater.inflate(R.layout.video_progress, null);
            }
            return mVideoProgressView;
        }

        @Override
        public void onHideCustomView() {
            super.onHideCustomView();    //To change body of overridden methods use File | Settings | File Templates.
            if (mCustomView == null)
                return;

            if(webView != null) webView.setVisibility(View.VISIBLE);
            customViewContainer.setVisibility(View.GONE);

            // Hide the custom view.
            mCustomView.setVisibility(View.GONE);

            // Remove the custom view from its container.
            customViewContainer.removeView(mCustomView);
            customViewCallback.onCustomViewHidden();

            mCustomView = null;
        }
    }

    class myWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);    //To change body of overridden methods use File | Settings | File Templates.
        }
    }

    public String getJavascript(String videoUrl){
        String javascript = "";

        if(videoUrl.contains("goo.gl")){
            javascript = "";
        } else if(videoUrl.contains("openload")){
            javascript = "$('#videooverlay').click()";
        }  else if(videoUrl.contains("rapidvideo")){
            javascript = "$('#home_video').click()";
        }  else if(videoUrl.contains("dailymotion")){
            javascript = "$('.np_ViewPoster').click()";
        } else if(videoUrl.contains("gdriveplayer")){   // ada url in video class
            javascript = "";
        } else if(videoUrl.contains("pandora")){    // 그냥 띄움
            javascript = "$('#sBtnBigPlay').click()";
        } else if(videoUrl.contains("streamango")){
            javascript = "$('.vjs-big-play-button').click()";
        } else if(videoUrl.contains("streamcherry")){
            javascript = "$('.vjs-big-play-button').click()";
        } else if(videoUrl.contains("mgoon.com")){
            javascript = "$('#playIcon').click()";
        } else if(videoUrl.contains("yutube.com")){
            javascript = "";
        } else if(videoUrl.contains("")){
            javascript = "";
        } else if(videoUrl.contains("")){
            javascript = "";
        } else if(videoUrl.contains("")){
            javascript = "";
        } else {
            javascript = "";    // webview로 그냥 던짐
        }
        return javascript;
    }

    public String getFileType(String videoUrl){
        String tempFileType = "webview";
        Log.d(TAG, "videoUrl : " + videoUrl);
        if(videoUrl.contains("goo.gl")){    // 바로 실행
            tempFileType = "html";
        } else if(videoUrl.contains("openload")){
            tempFileType = ".mp4";
        }  else if(videoUrl.contains("rapidvideo")){
            tempFileType = ".mp4";
        }  else if(videoUrl.contains("dailymotion")){
            tempFileType = ".m3u8";
        } else if(videoUrl.contains("gdriveplayer")){   // ada url in video class
            tempFileType = "html";
        } else if(videoUrl.contains("pandora")){    // 그냥 띄움
            tempFileType = "pandora.fuck";
        } else if(videoUrl.contains("naver")){
            tempFileType = "webview";
        } else if(videoUrl.contains("is.gd")){
            tempFileType = "html";
        } else if(videoUrl.contains("bit.ly")){
            tempFileType = "html";
        } else if(videoUrl.contains("streamango")){
            tempFileType = ".mp4";
        } else if(videoUrl.contains("streamcherry")){
            tempFileType = ".mp4";
        } else if(videoUrl.contains("mgoon.com")){
            tempFileType = ".mp4";
        } else if(videoUrl.contains("k-vid.net")){
            tempFileType = "video/mp4";
        } else if(videoUrl.contains("watchasian.io")){
            tempFileType = "video/mp4";
        } else if(videoUrl.contains("igenetive.com")){
            tempFileType = "html";
        } else if(videoUrl.contains("vidtodo.com")){
            tempFileType = "html";
        }  else if(videoUrl.contains("vidstodo.me")){
            tempFileType = "html";
        }  else if(videoUrl.contains("is.gd")){
            tempFileType = "html";
        }  else {
            tempFileType = "";    // webview로 그냥 던짐
        }
        return tempFileType;
    }

    public void startMxPlayer(String videoUrl){
        String mimeType = MimeTypeMap.getFileExtensionFromUrl(videoUrl);
        Log.d(TAG, "mime type : " + mimeType);  // flv, mp4

        autoCek = false;
        String packageName = "";

        Intent startLink1 = getPackageManager().getLaunchIntentForPackage("com.mxtech.videoplayer.ad");
        Intent startLink2 = getPackageManager().getLaunchIntentForPackage("com.mxtech.videoplayer.pro");

        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri videoUri = Uri.parse(videoUrl);
        intent.setDataAndType(videoUri, "video/h264");
        intent.putExtra("decode_mode", (byte) 2);
        intent.putExtra("video_zoom", 0);
        if (startLink2 != null) {
            packageName = "com.mxtech.videoplayer.pro";
        } else {
            packageName = "com.mxtech.videoplayer.ad";
        }
        intent.setPackage(packageName);
        startActivity(intent);
        finish();
    }

    public class MyJavascriptInterface {
        @JavascriptInterface
        public void getHtml(String html){
            if(html != null && !html.equals("")){
                Document doc = Jsoup.parse(html);
                String url = doc.select("video").attr("src");
                //startMxPlayer(url);
            }
        }
    }

    public void destroyWebView() {
        webView.clearHistory();
        webView.clearCache(true);
        webView.loadUrl("about:blank");
        webView.onPause();
        webView.removeAllViews();
        webView.destroyDrawingCache();
        webView.destroy();
        webView = null;
    }
}
