package com.freeking.drama3.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.freeking.drama3.R;
import com.freeking.drama3.adapter.ListViewMidAdapterPlayBtn;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class PlayListMidActivity extends Activity {
    private String TAG = " PlayListMidActivity - ";
    private ProgressDialog mProgressDialog;
    ListView listView;
    private GetListView getListView = null;
    private String baseUrl = "";

    private ImageView posterView;
    private TextView titleView;
    private TextView storyView;

    private ListView btnListView;

    private int adsCnt = 0;

    private String listUrl = "";
    private String nextUrl = "";
    private GetPlayerUrl getPlayerUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_play_list_mid);

        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        Intent intent = getIntent();
        baseUrl = intent.getStringExtra("listUrl");
        adsCnt = Integer.parseInt(intent.getStringExtra("adsCnt"));

        posterView = (ImageView)findViewById(R.id.iv_list_eps);
        titleView = (TextView)findViewById(R.id.tv_list_eps);
        storyView = (TextView)findViewById(R.id.tv_list_story);

        listView = (ListView)findViewById(R.id.list_listview);
        btnListView = (ListView)findViewById(R.id.list_btn_view);

        getListView = new GetListView();
        getListView.execute();
    }

    public class GetListView extends AsyncTask<Void, Void, Void> {

        String imgUrl = "";
        String title = "";
        String story = "";

        ArrayList<String> btnTextArr = new ArrayList<String>();
        List<String> btnVideoUrlArr = new ArrayList<String>();

        List<String> listTitleArr = new ArrayList<String>();
        List<String> listPageUrlArr = new ArrayList<String>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(PlayListMidActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                doc = Jsoup.connect(baseUrl).timeout(15000).get();

                ////////////////// episode list //////////////
                Elements lists = doc.select(".list-subject");

                for(int i=0 ; i<lists.size() ; i++){
                    String listTitle = lists.get(i).select("a").text();
                    String listVideoUrl = lists.get(i).select("a").attr("href");
                    listTitleArr.add(listTitle);
                    listPageUrlArr.add(listVideoUrl);
                }
                //////////////// episode contents ////////////
                imgUrl = doc.select(".thumbnail img").attr("abs:src");
                title = doc.select(".thumbnail .caption h2").text();
                story = doc.select(".thumbnail p").text();

                /////////////// video button //////////////
                Elements btns = doc.select(".row .col-md-8 center a");

                for(int i=0 ; i<btns.size()-1 ; i++){
                    String btnText = btns.get(i).text();
                    String btnVideoUrl = btns.get(i).attr("href");

                    btnTextArr.add(btnText);
                    btnVideoUrlArr.add(btnVideoUrl);

                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(!(imgUrl == null || imgUrl.equals(""))){
                Picasso.with(PlayListMidActivity.this).load(imgUrl).into(posterView);
            }
            titleView.setText(title);
            storyView.setText(story);

            //////////// set listview ///////////
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(PlayListMidActivity.this, android.R.layout.simple_list_item_1, listTitleArr);
            listView.setAdapter(adapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    listUrl = listPageUrlArr.get(position);
                    refresh(listUrl);
                }
            });

            ///////// set button list ////////////
            btnListView.setAdapter(new ListViewMidAdapterPlayBtn(btnTextArr));
            btnListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    nextUrl = btnVideoUrlArr.get(position);
                    getPlayerUrl = new GetPlayerUrl();
                    getPlayerUrl.execute();
                }
            });

            mProgressDialog.dismiss();
        }
    }

    public class GetPlayerUrl extends AsyncTask<Void, Void, Void> {

        String playerUrl = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(PlayListMidActivity.this);
            mProgressDialog.setTitle("플레이어를 찾는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }
        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;

            try {
                doc = Jsoup.connect(nextUrl).timeout(15000).get();
                playerUrl = doc.select("iframe").attr("src");
                Log.d(TAG, "playerUrl : " + playerUrl);

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(playerUrl.contains("k-vid")){
                Toast.makeText(PlayListMidActivity.this, "이 링크는 사용할 수 없습니다.", Toast.LENGTH_LONG).show();
            } else {
                Intent intent;
                if(playerUrl.contains("pandora")){
                    intent = new Intent(PlayListMidActivity.this, WebviewPlayer.class);
                    intent.putExtra("videoUrl", playerUrl);
                    Toast.makeText(PlayListMidActivity.this, "판도라TV는 광고를 제거 할 수 없습니다.", Toast.LENGTH_LONG).show();
                } else {
                    intent = new Intent(PlayListMidActivity.this, GetMediaFileActivity.class);
                    intent.putExtra("baseUrl", playerUrl);
                }
                startActivity(intent);
            }

            mProgressDialog.dismiss();
        }
    }

    public void refresh(String listUrl){
        Intent intent = new Intent(PlayListMidActivity.this, PlayListActivity.class);
        intent.putExtra("listUrl",  "https://www.otgtv.net" + listUrl);
        intent.putExtra("adsCnt",  adsCnt+"");
        finish();
        startActivity(intent);
    }

    public void destroyAsync(){
        if(getListView != null){
            getListView.cancel(true);
        }
        if(getPlayerUrl != null){
            getPlayerUrl.cancel(true);
        }
    }
}
